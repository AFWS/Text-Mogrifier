# Text-Mogrifier, Version: 1.2.0b

/*
 *	APPLICATION TITLE: TEXT MOGRIFIER (CLI version)
 *  PURPOSE: To alter a line text, by substituting alphabetics with similar-looking characters.
 *	AUTHOR: Jim S. Smith
 *	COPYRIGHT: (c)2022 - A FRESH WEB SOLUTION, Licensed, with attribution, under GPL 2.0 or later.
 *
 *	VERSION: 1.2.0a, Updated: 2022/09/19
 *
 *  DESCRIPTION: An application - whose purpose is to alter a line of text by substituting
 *  alphabetic characters with similar-looking ones - using the extended character sets. These
 *  other characters are called "Homoglyphs", because of their near-similar appearance to the
 *  actual characters they are replacing.
 *
 *  - This application was developed as a "concept idea" and for fun, not intended for real-world uses!
 *
 *  Therefore,
 *
 *  All responsibility from results of the use of this application is entirely YOURS!
 *
 *  - USE AT YOUR OWN RISK!
 *
 *  REQUIREMENTS:
 *
 *  1. PHP version of at least 7.3 or greater.
 *  2. Ability to use Extended character sets,
 *
 */