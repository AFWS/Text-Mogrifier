Text-Mogrifier

This little script, because there are usually more than one "homoglyph" to each character, can produce different combinations of homoglyphs with each iteration. This is randomized for effect.

This script also differentiates between lowercase and UPPERCASE characters, as well as numbers, other symbols, and punctuation.


How to use both the CLI port, and the Web port.

1. The CLI port is very easy to use - and only requires that you have PHP-CLI installed and enabled. Best to use versions 7.3 and newer. The only real requirement: That your system can use extended characters, AND the particular Font that you use may provide different results, but any standard "Monospace" font should work.


To Use:  mogrify-text "The string of text you wish to convert."


Example Results:

	Ⱦḫȅ šṭŗϊиģ ởғ țὲẍṱ ӱὂũ ẁịšḧ ṯȭ ƈȭҋṿḙŕţ.

	Τḧέ ṡţřīǹǵ ɵƒ ƭḗẋṭ ýóṹ ẉíṥḧ țổ ȼөňṿèŗṭ.

You can run the same line of text multiple times, and still get different results each time!


2. The Web port requires you to have Apache2 (or compatible SAPI) running and with either "mod_php7"-module or "php-fpm" with "mod_fcgi_proxy" installed and running.


By simply accessing http(s?)://URL/text-mogrifier.php from the browser, you will be presented with a Form to enter your line of text, and upon clicking the "Send" button, given your result in the results box.


As with the CLI port,

Each "send" of the same text will yield different results!

