<?php @ob_start( 'ob_gzhandler' );	//	Compress output, if user's browser supports it.

/*
 *	APPLICATION TITLE: TEXT MOGRIFIER (WEB version)
 *  PURPOSE: To alter a line text, by substituting alphabetics with similar-looking characters.
 *	AUTHOR: Jim S. Smith
 *	COPYRIGHT: (c)2022 - A FRESH WEB SOLUTION, Licensed, with attribution, under GPL 2.0 or later.
 *
 *	VERSION: 1.2.0a, Updated: 2022/09/19
 *
 *  DESCRIPTION: An application - whose purpose is to alter a line of text by substituting
 *  alphabetic characters with similar-looking ones - using the extended character sets. These
 *  other characters are called "Homoglyphs", because of their near-similar appearance to the
 *  actual characters they are replacing.
 *
 *  - This application was developed as a "concept idea" and for fun, not intended for real-world uses!
 *
 *  Therefore,
 *
 *  All responsibility from results of the use of this application is entirely YOURS!
 *
 *  - USE AT YOUR OWN RISK!
 *
 *  REQUIREMENTS:
 *
 *  1. PHP version of at least 7.3 or greater.
 *  2. Ability to use Extended character sets,
 *
 */


/* *	APPLICATION INFO.	* */
define( 'APPLICATION', 'TEXT MOGRIFIER' );
define( 'VERSION', '1.2.0a' );
define( 'RELEASE_DATE', date( 'Y-m-d', filemtime( __FILE__ ) ) );
define( 'AUTHOR', 'Jim S. Smith' );
define( 'COPYRIGHT', 'A FRESH WEB SOLUTION' );
define( 'DESCRIPTION', 'To alter a line of text, by substituting alphabetics with similar-looking characters ("homoglyphs").' );

//	OTHER DEFINES AND VARIABLES.
define( 'CHARSET', @ini_get( 'default_charset' ) );

//	WE WILL CREATE AND USE OUR OWN ENVIRONMENT SUPERGLOBALS.
unset( $_ENV );

//	MAKE SURE WE GET AT LEAST A VALID "TMP" PATH LOCATION!
if ( ! defined( 'TEMP_PATH' ) ) {
	if ( ! @empty( ini_get( 'upload_tmp_dir' ) ) ) define( 'TEMP_PATH', ini_get( 'upload_tmp_dir' ) );
	else define( 'TEMP_PATH', sys_get_temp_dir() );
	}
$_ENV['TEMP_PATH'] = TEMP_PATH;

$this_app = htmlspecialchars( $_SERVER['REQUEST_URI'], ENT_QUOTES, 'utf-8' );


//	ANTI-XSS SAFEGUARD.
if ( ! function_exists( 'no_xss' ) ) {
	function no_xss( $data ) {
		if ( ! empty( $data ) ) return htmlspecialchars( trim( strip_tags( trim( $data ) ) ), ENT_QUOTES, CHARSET );
		}
	}


//	DEFEND AGAINST XSS-ATTACKS.
$env_keys = array( 'SERVER_NAME', 'HTTP_USER_AGENT', 'HTTP_REFERER', 'REQUEST_URI', 'HTTP_HOST', 'REMOTE_ADDR', 'ACCEPT_CHARSET' );

foreach ( $env_keys as $this_key => $k_val ) if ( isset( $_SERVER[$this_key] ) ) $_SERVER[$this_key] = no_xss( $_SERVER[$this_key] );
if ( isset( $_POST ) && is_array( $_POST ) ) foreach ( $_POST as $this_key => $k_val ) $_POST[$this_key] = no_xss( $_POST[$this_key] );
if ( isset( $_GET ) && is_array( $_GET ) ) foreach ( $_GET as $this_key => $k_val ) $_GET[$this_key] = no_xss( $_GET[$this_key] );


if ( ! defined( 'WHERE_AM_I' ) ) define( 'WHERE_AM_I', $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] );
$_ENV['THIS_APP'] = 'http' . ( isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == 'on' ? 's' : '' ) . '://' . WHERE_AM_I;


/* *	DEFINED FUNCTIONS AND PROTOTYPES (Objects, etc.).	* */

$max_chars = 250;


//	Create the array to hold the homoglyph replacements.
function init_homoglyph_array() {
//	"UPPERCASE" set.
	$the_chars = [
		'A'	=>	[ 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ă', 'Ą', 'Ǎ', 'Ǟ', 'Ȃ', 'Ȧ', 'Α', 'А', 'Ḁ', '₳' ],
		'B'	=>	[ 'Ƀ', 'Β', 'В', 'Ḇ', 'ẞ', 'Ɓ', 'Ḃ', 'Ḅ' ],
		'C'	=>	[ 'Ç', 'Ć', 'Ĉ', 'Ċ', 'Č', 'Ƈ', 'Ȼ', 'Ϛ', 'Ϲ', 'С', 'Ҁ', 'Ҫ', 'Ḉ', '₡', '₵' ],
		'D'	=>	[ 'Ð', 'Ď', 'Đ', 'Ɖ', 'Ɗ', 'Ḋ', 'Ḍ', 'Ḏ', 'Ḑ', 'Ḓ' ],
		'E'	=>	[ 'È', 'É', 'Ê', 'Ë', 'Ē', 'Ĕ', 'Ė', 'Ę', 'Ě', 'Ȅ', 'Ȇ', 'Ȩ', 'Ɇ', 'Έ', 'Ε', 'Σ', 'Ѐ', 'Ё', 'Е', 'Ӗ', 'Ḕ', 'Ḗ', 'Ḙ', 'Ḛ', 'Ḝ', 'Ẽ', 'Ế', 'Ề', 'Ể', 'Ễ', 'Ệ', 'Ἐ', 'Ἑ', 'Ἒ', 'Ἓ', 'Ἔ', 'Ἕ', 'Ὲ', 'Έ' ],
		'F'	=>	[ 'Ƒ', 'Ғ', 'Ӻ', 'Ḟ', '₣' ],
		'G'	=>	[ 'Ĝ', 'Ğ', 'Ġ', 'Ģ', 'Ɠ', 'Ǥ', 'Ǧ', 'Ǵ', 'Ḡ', '₲' ],
		'H'	=>	[ 'Ĥ', 'Ħ', 'Ƕ', 'Ȟ', 'Ή', 'Η', 'Н', 'Ӈ', 'Ӊ', 'Ḣ', 'Ḥ', 'Ḧ', 'Ḩ', 'Ḫ', 'Ἠ', 'Ἡ', 'Ἢ', 'Ἣ', 'Ἤ', 'Ἥ', 'Ἦ', 'Ἧ', 'ᾘ', 'ᾙ', 'ᾚ', 'ᾛ', 'ᾜ', 'ᾝ', 'ᾞ', 'ᾟ', 'Ⱨ' ],
		'I'	=>	[ 'Ì', 'Í', 'Î', 'Ï', 'Ĩ', 'Ī', 'Ĭ', 'Į', 'İ', 'Ɨ', 'Ǐ', 'Ȉ', 'Ȋ', 'Ί', 'Ι', 'Ϊ', 'Ї', 'Ӏ', 'Ḭ', 'Ḯ', 'Ỉ', 'Ị', 'Ἰ', 'Ἱ', 'Ἲ', 'Ἳ', 'Ἴ', 'Ἵ', 'Ἶ', 'Ἷ', 'Ῐ', 'Ῑ', 'Ὶ', 'Ί' ],
		'J'	=>	[ 'ĵ', 'ǰ', 'Ɉ', 'Ј' ],
		'K'	=>	[ 'Ķ', 'Ƙ', 'Ǩ', 'Κ', 'Қ', 'Ҝ', 'Ҟ', 'Ҡ', 'Ḱ', 'Ḳ', 'Ḵ', '₭', 'Ⱪ' ],
		'L'	=>	[ 'Ĺ', 'Ļ', 'Ľ', 'Ŀ', 'Ł', 'Ḷ', 'Ḹ', 'Ḻ', 'Ḽ', '₤', 'Ɫ' ],
		'M'	=>	[ 'Μ', 'Ϻ', 'М', 'Ӎ', 'Ḿ', 'Ṁ', 'Ṃ' ],
		'N'	=>	[ 'Ń', 'Ņ', 'Ň', 'Ŋ', 'Ɲ', 'Ǹ', 'Ν', 'Ѝ', 'И', 'Й', 'Ҋ', 'Ṅ', 'Ṇ', 'Ṉ', 'Ṋ', '₦' ],
		'O'	=>	[ 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ō', 'Ŏ', 'Ő', 'Ɵ', 'Ơ', 'Ǒ', 'Ȍ', 'Ȏ', 'Ȫ', 'Ȭ', 'Ȯ', 'Ȱ', 'ʘ', 'Ό', 'Θ', 'Ο', 'ϴ', 'О', 'Ѳ', 'Ѻ', 'Ӧ', 'Ө', 'Ӫ', 'Ṍ', 'Ṏ', 'Ṑ', 'Ṓ', 'Ọ', 'Ỏ', 'Ố', 'Ồ', 'Ổ', 'Ỗ', 'Ộ', 'Ớ', 'Ờ', 'Ở', 'Ỡ', 'Ợ', 'Ὀ', 'Ὁ', 'Ὂ', 'Ὃ', 'Ὄ', 'Ὅ', 'Ὸ', 'Ό' ],
		'P'	=>	[ 'Ƥ', 'Ρ', 'Р', 'Ҏ', 'Ṕ', 'Ṗ', '₱', 'Ᵽ' ],
		'Q'	=>	[ 'Ǫ', 'Ǭ', 'Ԛ' ],
		'R'	=>	[ 'Ŕ', 'Ŗ', 'Ř', 'Ȑ', 'Ȓ', 'Ɍ', 'Ṙ', 'Ṛ', 'Ṝ', 'Ṟ', 'Ɽ' ],
		'S'	=>	[ '$', 'Ś', 'Ŝ', 'Ş', 'Š', 'Ș', 'Ṡ', 'Ṣ', 'Ṥ', 'Ṧ', 'Ṩ', '₴' ],
		'T'	=>	[ 'Ţ', 'Ť', 'Ŧ', 'Ƭ', 'Ʈ', 'Ț', 'Ⱦ', 'Τ', 'Т', 'Ҭ', 'Ṫ', 'Ṭ', 'Ṯ', 'Ṱ', '₮' ],
		'U'	=>	[ 'Ù', 'Ú', 'Û', 'Ü', 'Ũ', 'Ū', 'Ŭ', 'Ů', 'Ű', 'Ų', 'Ʋ', 'Ǔ', 'Ǖ', 'Ǘ', 'Ǚ', 'Ǜ', 'Ȕ', 'Ȗ', 'Ʉ', 'Ṳ', 'Ṵ', 'Ṷ', 'Ṹ', 'Ṻ', 'Ụ', 'Ủ', 'Ứ', 'Ừ', 'Ử', 'Ữ', 'Ự' ],
		'V'	=>	[ 'Ɣ', 'Ṽ', 'Ṿ', 'Ѵ', 'Ѷ' ],
		'W'	=>	[ 'Ŵ', 'Ԝ', 'Ẁ', 'Ẃ', 'Ẅ', 'Ẇ', 'Ẉ', 'Ⱳ' ],
		'X'	=>	[ 'Χ', 'Ẋ', 'Ẍ' ],
		'Y'	=>	[ 'Ý', 'Ŷ', 'Ÿ', 'Ɏ', 'Ύ', 'Υ', 'Ў', 'У', 'Ү', 'Ұ', 'Ӯ', 'Ӱ', 'Ӳ', 'Ẏ', 'Ỳ', 'Ỵ', 'Ỷ', 'Ỹ', 'Ὑ', 'Ὓ', 'Ὕ', 'Ὗ', 'Ῠ', 'Ῡ', 'Ὺ', 'Ύ' ],
		'Z'	=>	[ 'Ź', 'Ż', 'Ž', 'Ƶ', 'Ȥ', 'Ẑ', 'Ẓ', 'Ẕ', 'Ⱬ' ],

//	"lowercase" set.
		'a'	=>	[ 'à', 'á', 'â', 'ã', 'ä', 'å', 'ă', 'ą', 'ǎ', 'ǟ', 'ȃ', 'ȧ', 'α', 'а', 'ḁ' ],
		'b'	=>	[ 'ƀ', 'ḇ', 'ɓ', 'ḃ', 'ḅ' ],
		'c'	=>	[ 'ç', 'ć', 'ĉ', 'ċ', 'č', 'ƈ', 'ȼ', 'ϛ', 'ϲ', 'с', 'ҁ', 'ҫ', 'ḉ' ],
		'd'	=>	[ 'ð', 'ď', 'đ', 'ɖ', 'ɗ', 'ḋ', 'ḍ', 'ḏ', 'ḑ', 'ḓ' ],
		'e'	=>	[ 'è', 'é', 'ê', 'ë', 'ē', 'ĕ', 'ė', 'ę', 'ě', 'ȅ', 'ȇ', 'ȩ', 'ɇ', 'έ', 'ε', 'ѐ', 'ё', 'е', 'ӗ', 'ḕ', 'ḗ', 'ḙ', 'ḛ', 'ḝ', 'ẽ', 'ế', 'ề', 'ể', 'ễ', 'ệ', 'ἐ', 'ἑ', 'ἒ', 'ἓ', 'ἔ', 'ἕ', 'ὲ', 'έ' ],
		'f'	=>	[ 'ƒ', 'ғ', 'ḟ' ],
		'g'	=>	[ 'ĝ', 'ğ', 'ġ', 'ģ', 'ɠ', 'ǥ', 'ǧ', 'ǵ', 'ḡ' ],
		'h'	=>	[ 'ĥ', 'ħ', 'ƕ', 'ȟ', 'ḣ', 'ḥ', 'ḧ', 'ḩ', 'ḫ', 'ⱨ' ],
		'i'	=>	[ 'ì', 'í', 'î', 'ï', 'ĩ', 'ī', 'ĭ', 'į', 'i', '̇ɨ', 'ǐ', 'ȉ', 'ȋ', 'ί', 'ι', 'ϊ', 'ї', 'ḭ', 'ḯ', 'ỉ', 'ị', 'ἰ', 'ἱ', 'ἲ', 'ἳ', 'ἴ', 'ἵ', 'ἶ', 'ἷ', 'ῐ', 'ῑ', 'ὶ', 'ί' ],
		'j'	=>	[ 'ĵ', 'ǰ', 'ј' ],
		'k'	=>	[ 'ķ', 'ƙ', 'ǩ', 'κ', 'қ', 'ḱ', 'ḳ', 'ḵ', 'ⱪ' ],
		'l'	=>	[ 'ĺ', 'ļ', 'ľ', 'ŀ', 'ł', 'ḷ', 'ḹ', 'ḻ', 'ḽ', 'ɫ' ],
		'm'	=>	[ 'ϻ', 'ḿ', 'ṁ', 'ṃ' ],
		'n'	=>	[ 'ń', 'ņ', 'ň', 'ŋ', 'ɲ', 'ǹ', 'ѝ', 'и', 'й', 'ҋ', 'ṅ', 'ṇ', 'ṉ', 'ṋ' ],
		'o'	=>	[ 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ō', 'ŏ', 'ő', 'ɵ', 'ơ', 'ǒ', 'ȍ', 'ȏ', 'ȫ', 'ȭ', 'ȯ', 'ȱ', 'ό', 'ο', 'о', 'ѳ', 'ѻ', 'ӧ', 'ө', 'ӫ', 'ṍ', 'ṏ', 'ṑ', 'ṓ', 'ọ', 'ỏ', 'ố', 'ồ', 'ổ', 'ỗ', 'ộ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ', 'ὀ', 'ὁ', 'ὂ', 'ὃ', 'ὄ', 'ὅ', 'ὸ', 'ό' ],
		'p'	=>	[ 'ƥ', 'ρ', 'р', 'ṕ', 'ṗ', 'ᵽ' ],
		'q'	=>	[ 'ǫ', 'ǭ', 'ԛ' ],
		'r'	=>	[ 'ŕ', 'ŗ', 'ř', 'ȑ', 'ȓ', 'ɍ', 'ṙ', 'ṛ', 'ṝ', 'ṟ', 'ɽ' ],
		's'	=>	[ 'ś', 'ŝ', 'ş', 'š', 'ș', 'ṡ', 'ṣ', 'ṥ', 'ṧ', 'ṩ' ],
		't'	=>	[ 'ţ', 'ť', 'ŧ', 'ƭ', 'ʈ', 'ț', 'ⱦ', 'ṫ', 'ṭ', 'ṯ', 'ṱ' ],
		'u'	=>	[ 'ù', 'ú', 'û', 'ü', 'ũ', 'ū', 'ŭ', 'ů', 'ű', 'ų', 'ʋ', 'ǔ', 'ǖ', 'ǘ', 'ǚ', 'ǜ', 'ȕ', 'ȗ', 'ʉ', 'ṳ', 'ṵ', 'ṷ', 'ṹ', 'ṻ', 'ụ', 'ủ', 'ứ', 'ừ', 'ử', 'ữ', 'ự' ],
		'v'	=>	[ 'ṽ', 'ṿ', 'ѵ', 'ѷ' ],
		'w'	=>	[ 'ŵ', 'ԝ', 'ẁ', 'ẃ', 'ẅ', 'ẇ', 'ẉ', 'ⱳ' ],
		'x'	=>	[ 'χ', 'ẋ', 'ẍ' ],
		'y'	=>	[ 'ý', 'ŷ', 'ÿ', 'ɏ', 'ў', 'у', 'ү', 'ұ', 'ӯ', 'ӱ', 'ӳ', 'ẏ', 'ỳ', 'ỵ', 'ỷ', 'ỹ' ],
		'z'	=>	[ 'ź', 'ż', 'ž', 'ƶ', 'ȥ', 'ẑ', 'ẓ', 'ẕ', 'ⱬ' ],
		];

//	Randomize the position of the homoglyphs in each array column.
	foreach ( $the_chars as $this_char => $the_string_set ) {
		shuffle( $the_chars[$this_char] );
		}

	return $the_chars;
}

function trans_mogrify_Text( $text_data = '' ) {
	$str_leng = strlen( $text_data );

	$out_text = '';

	for ( $i = 0; $i < $str_leng; $i++ ) {
		$this_char = $text_data[$i];

		if ( preg_match( '#[a-z]#i', $this_char ) ) {
			$rnd_top = count( $_ENV['the_chars'][$this_char] );
			$new_point = random_int( 0, ( $rnd_top - 1 ) );
			$out_text .= $_ENV['the_chars'][$this_char][$new_point];
			}

		else {
			$out_text .= $this_char;
			}
		}

	return $out_text;
}


/* *	BEGIN THE MAIN PROGRAM (Handle FORM inputs and prepared data, perhaps?).	* */

$_ENV['the_chars'] = init_homoglyph_array();

$_POST['in-text'] = $in_text = ( isset( $_POST['in-text'] ) ? htmlspecialchars_decode( $_POST['in-text'], ENT_QUOTES ) : '' );
$out_text = "";

if ( ! empty( $_POST['in-text'] ) ) {
	if ( strlen( $in_text ) > $max_chars ) {
		$in_text = substr(  $in_text, 0, $max_chars );
		}

	$out_text = trans_mogrify_Text( preg_replace( '#[^_a-z0-9.?!,\ ()\'":;{}\[\]\s&$%+/\*\#@=-].#si', '', $in_text ) );
	}


$app_title = APPLICATION;
$app_desc = DESCRIPTION;
$app_version = VERSION;
$app_update = RELEASE_DATE;
$app_charset = CHARSET;


$example_word = '"Censored" word or phrase.';
$example_mogrified = trans_mogrify_Text( $example_word );

$example_word2 = 'Now is The Time, for All Good Men, to Come to The Aid of Their Country!';
$example_mogrified2 = trans_mogrify_Text( $example_word2 );


/* *	BEGIN HTML PAGE.	* */

header( 'Content-Type: text/html; charset=' . CHARSET );

?><!DOCTYPE html>
<html lang="en" dir="ltr" id="the-HTML-tag">
<head class="web_applications">
	<meta name="charset" content="<?php echo CHARSET; ?>"><?php /*	<!-- Some browsers still require this for HTML validation! -->	*/ ?>
<?php /*	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">	*/ ?>

	<title><?php echo APPLICATION; ?>, V<?php echo VERSION; ?></title>

	<meta name="generator" content="<?php echo APPLICATION; ?>, Ver. <?php echo VERSION; ?>">
	<meta name="author" content="<?php echo AUTHOR; ?>">
	<meta name="copyright" content="(c)<?php echo date( 'Y', filemtime( __FILE__ ) ); ?> <?php echo COPYRIGHT; ?>">
	<meta name="description" content='<?php echo DESCRIPTION; ?>'>
	<meta name="x:revised" content="<?php echo date( 'Y/m/d', filemtime( __FILE__ ) ); ?>">

	<link href="" rel="favicon" type="image/x-icon"><!--	Load site "favicon" here.	-->
	<link href="" rel="fonts" type="font/styles"><!--	Load external fonts here.	-->
	<link href="" rel="stylesheet" type="text/css" media="all"><!--	Load external Stylesheet here.	-->

	<style type='text/css'><!-- /* <![CDATA[ */

		* { font-size: 1.0em,16px; }
		.centered { text-align: center; }

/*		form { width: 600px; margin: 0 auto; }	*/
		table { border: 0; border-collapse: collapse; width: 85%; margin: 0 auto; }
		form { width: 95%; margin: 0 auto; }
/*		textarea { width: 450px; height: 100px; background: #efefef; color: #000; padding: 2px; font-family: Monospace,Verdana,Roboto; font-size: 0.9555em; }	*/
		textarea { width: 99.55555555%; height: 100px; background: #efefef; color: #000; padding: 2px; font-family: Monospace,Verdana,Roboto; font-size: 0.9555em; }
		td { text-align: center; }
		input { font-weight: 600; }

		.panel { float: left; width: 48%; max-width: 610px; min-width: 300px; }
		.reset-this { clear: both; }

		.bolded { font-weight: 700; }
		.spacer { padding: 5px 0; }
		.red { color: #c22; }
		.green { color: #1a1; }
		.blue { color: #22e; }

	/* ]]> */ --></style>

	<script>

		window.onload = function() {

			var theForm = document.getElementById("the-form");
			var theInText = document.getElementById("in-text");
			var theOutText = document.getElementById("out-text");
			var theResetButton = document.getElementById("reset-me");

			var getLength;
			var theDefaultBG = theInText.style.backgroundColor;

			theResetButton.onclick = function() {
/*
				theInText.value = '';
				theOutText.value = '';
*/
				document.location = '<?php echo $_ENV['THIS_APP']; ?>';
				return false;
			};


			theInText.onblur = function() {
				getLength = theInText.value.length;

				if (getLength > <?php echo $max_chars; ?>) {
/*					alert('Text to be submitted is longer than $max_chars characters in length!\n\nInput will be truncated to the maximum allowed length.\n\n'); */

					theInText.style.backgroundColor = '#ffdddd';
					return false;
					}

				else {
					theInText.style.backgroundColor = theDefaultBG;
					}
			};


			theForm.onsubmit = function() {
				getLength = theInText.value.length;

				if (getLength > <?php echo $max_chars; ?>) {
					alert('Text to be submitted is longer than <?php echo $max_chars; ?> characters in length!\n\nInput will be truncated to the maximum allowed length.\n\n');

/*					return false;	*/
					}
			};

		};

	</script>

</head>
<?php /*	DO SOMETHING HERE (Perhaps some application preparations, or something?) . . .	*/ ?>

<body id="Im-the-BODY">

<?php echo ""; ?>

<?php echo <<< _HTML_
	<h1 class="centered">$app_title, Version: $app_version</h1>
	<h2 class="centered">$app_desc</h2>
	<hr>

	<div class="panel">
		<p>&nbsp;&nbsp;This application transforms the text input into one using what are called "homoglyphs" in place of the original Latin alphabetic characters, while leaving intact the punctuation, numbers, etc. This version also takes into account the character cases when doing the conversions. The "homoglyphs" are selected at random for recurring characters, thus providing a random output string each time.</p>

		<p>&nbsp;&nbsp;May also be helpful in bypassing <u>some forms or methods of "word-censorship"</u> on some boards, forums, article comments, etc?</p>

		<h4>Example 1: <b class="blue">$example_word</b> =&gt; <b class="green">$example_mogrified</b></h4>
		<h4>Example 2:<br><br><b class="blue">$example_word2</b> =&gt;<br><br><b class="green">$example_mogrified2</b></h4><hr>
	</div>

	<div class="panel">
		<form action="$this_app" method="post" id="the-form">
			<table>
				<caption><h3>Enter your text.</h3><p>Maximum character count is: <b>$max_chars</b>.</p><p>(<i class="red">Output will truncated, if longer than this.</i>)</p></caption>

				<tr><td colspan="2" class="bolded">In-text:</td></tr>

				<tr><td colspan="2"><textarea name="in-text" id="in-text" placeholder="Type/Paste your plain-text here. . .">{$_POST['in-text']}</textarea></td></tr>

				<tr class="spacer"><td colspan="2"><hr><br></td></tr>

				<tr>
					<td><input type="submit" name="send" value="Send" class="green"></td>
					<td><input type="submit" name="reset" id="reset-me" value="Clear" class="red"></td>
				</tr>

				<tr class="spacer"><td colspan="2"><hr><br></td></tr>

				<tr><td colspan="2" class="bolded">Out-Text ("Mogrified"):</td></tr>

				<tr><td colspan="2"><textarea name="out-text" id="out-text" placeholder="Your 'Mogrified' displayed here. . ." readonly title=" This box is 'read only', and displays what is sent back by the server. ">$out_text</textarea></td></tr>
			</table>
		</form>
	</div>

	<br class="reset-this">

	<p>&nbsp;</p>
	<p>&nbsp;</p>

_HTML_;

?>

<?php echo "<br><br><hr><h6>Copyright: <a href='https://dev-tools/'>Jim S Smith, 2022-09-19, of: A FRESH WEB SOLUTION, LLC</a></h6><br>"; ?>
</body>

</html>

<?php /* *	END HTML PAGE.	* */

@ob_end_flush();	//	Finish all output to user's browser.
exit();
